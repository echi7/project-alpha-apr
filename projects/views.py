from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProject


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    task = Project.objects.get(id=id)
    context = {
        "tasks": task,
    }
    return render(request, "projects/detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            new_project = form.save(False)
            new_project.owner = request.user
            new_project.save()
            return redirect("list_projects")
    else:
        form = CreateProject()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
